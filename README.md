# Block Title Access

This module allows you to get the CMS Static Block title from a block to use
in your templates.

In your templates, you can use it like this:

    <h2><?php echo $this->getChild("details")->getTitle() ?></h2>

    <div class="island">
        <div class="island">
            <?php echo $this->getChildHtml("details") ?>
        </div>
    </div>

Note I'm not doing `getChildHtml`, but `getChild` and calling `getTitle()` on it.

This is very handy - it allows the client to make changes to the website by editing
the CMS Static Block title and gives you greater flexibility over the HTML.