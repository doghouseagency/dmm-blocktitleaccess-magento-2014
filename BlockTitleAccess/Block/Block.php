<?php

class Doghouse_BlockTitleAccess_Block_Block extends Mage_Cms_Block_Block
{

    public function setBlockId($blockId) {
        $this->_block = Mage::getModel('cms/block')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($blockId);

        $this->setTitle($this->_block->getTitle());
	parent::setBlockId($blockId);

        return $this;
    }

    /**
     * Prepare Content HTML
     *
     * Edited to add the block model title in the block because we might
     * need this in our templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $blockId = $this->getBlockId();
        $html = '';
        if ($this->_block) {

            if ($this->_block->getIsActive()) {
                /* @var $helper Mage_Cms_Helper_Data */
                $helper = Mage::helper('cms');
                $processor = $helper->getBlockTemplateProcessor();
                $html = $processor->filter($this->_block->getContent());
                $this->addModelTags($this->_block);
            }
        }
        return $html;
    }
}
